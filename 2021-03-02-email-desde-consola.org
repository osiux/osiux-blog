#+TITLE:       email desde consola
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2021-03-02 23:39
#+HTML_HEAD:   <meta property="og:title" content="email desde consola" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2021-03-02" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2021-03-02-email-desde-consola.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/imapfilter-imap-offlineimap-maildir-mutt-smtp.png" />



#+ATTR_HTML: :width 640 :height 42 :title imapfilter imap offlineimap maildir mutt smtp
[[file:img/imapfilter-imap-offlineimap-maildir-mutt-smtp.png][file:tmb/imapfilter-imap-offlineimap-maildir-mutt-smtp.png]]

Enviar y recibir correos desde la consola, se puede realizar de manera
simple utilizando únicamente =mutt=, pero hay diferentes alternativas y
con el tiempo fui armando un ecosistema de aplicaciones que ha resistido
varias migraciones de servidores de correos, además de permitir trabajar
con múltiples cuentas, cifrando correos, usarlo detrás de un tunel =SSH=
y pudiendo realizar búsquedas extremadamente rápidas y resistiendo malas
conexiones al punto de contar con todos tus correos sin conexión alguna
estando totalmente /offline/.

A lo largo de varios /posts/ que seguramente me llevarán varios días,
iré documentando cada componente de mi entorno de correos desde consola.

** TODO vamos a filtrar los mails...

=imapfilter=

** TODO no necesito estar online

=offlineimap=

** TODO directorios y archivos

=maildir=

** TODO en que correo estaba eso...?

=notmuch=

** TODO el mejor de los /MUA/

=mutt=

** TODO recibo mas de lo que envío

=smtp=



** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/aac1e02095fa924540e0378f3935d1c9cbf453e3][=2021-03-02 23:49=]] agregar /email desde consola/
