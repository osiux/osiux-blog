#+TITLE:       Reemplazando *Org-mode* por =txt-bash-jrnl=
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2018-01-01 02:06
#+HTML_HEAD:   <meta property="og:title" content="Reemplazando *Org-mode* por =txt-bash-jrnl=" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2018-01-01" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2018-01-01-reemplazando-orgmode-por-txt-bash-jrnl.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />


En estos últimos 2 años dejé un poco de ser developer para convertirme
en un sysadmin y yo siempre dije *"como sysadmin soy buen developer y
como developer buen sysadmin"*, hoy lo más cercano sería un "devops".

Y este cambio de rol me obligó a cambiar mucho las herramientas con las
cuales trabajo a diario, ya no podía sostener el estar con un /EMACS/
abierto todo el día en un único archivo de proyecto /Org-mode/ [fn:orgmode],
aunque al principio lo intenté, se tornó extremadamente difícil mantener
la misma dinámica ya que como sysadmin ahora estoy constantemente en
varias terminales al mismo tiempo y cambiando de contexto todo el
tiempo.

Antes la secuencia era comenzar el día con una tarea grande e ir
desarrollándola durante el transcurso del día y tal vez continuarla
durante varios días hasta terminarla.

Hoy comienzo con una tarea a los poco minutos tengo que abandonarla por
otra más urgente y tal vez hasta interrumpir esta segunda tarea por otra
de mayor prioridad y me resulta vital llevar un registro de cada tarea,
en parte porque algunas van directamente al gestor de tareas redmine
para referencia o seguimiento y otras las quiero documentar para uso
futuro, si necesito volver en el tiempo y reutilizar lo aprendido.

Esta situación conllevó a volverme minimalista y lo primero que
encontré fue una aplicación realizada en /python/
llamada =jrnl= [fn:jrnl], la cual me resulto genial, lo único que
hacía era invocar mi editor de texto favorito /ViM/ [fn:vim]
agregándole un timestamp, entonces podía registrar libremente lo que
hacía, por lo general la salida de consola a algún comando y algún
texto de referencia, pero con el tiempo la noté un poco lenta ya que
guardaba cada entrada en un único archivo de texto, asi que decidí
realizar una alternativa en bash que almacenara en diferentes archivos
en una clásica estructura de árbol de directorios y esto me permitió
además agregarle diferentes funcionalidades que me iba necesitando y
que se tornaban cómodas para el uso cotidiano.

Asi llegué a publicar =txt-bash-jrnl= [fn:txt-bash-jrnl], el cual hoy
decidí publicar grandes cambios que tenía dando vueltas sin commitear
y en parte se debe a que este año quiero mantener actualizado este
blog, tengo muchísimo por publicar, ya que tengo unos 2 años de
journals para seleccionar, corregir y liberarlos.

Antes mi blog estaba desarrollado en /Org-mode/ y gran parte de él aún
sigue sin convertir de =.org= a =.txt= y por ello se me ocurrió que la
mejor manera sería utilizar un journal para mi blog y escribiendo en
formato markdown solo bastaría convertir a html cada journal para que
se conviertan en posts.

Trataré de publicar rápido y seguido, posiblemente en una modalidad de
micro-blog o mini posts mientras voy puliendo el /jrnl2blog/.

Es una excelente manera de comenzar este *Happy GNU Year* :)

[fn:jrnl]          http://jrnl.sh/
[fn:txt-bash-jrnl] https://github.com/osiris/txt-bash-jrnl/
[fn:orgmode]       http://orgmode.org/
[fn:vim]           http://www.vim.org/

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/882b1cda084f7c18362c8414a78dd65c7b968a3f][=2019-04-09 03:01=]] Recuperar archivos en .md y convertirlos a .org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/7baf690685f092fdd19f8a8aea2e92d49b4f6299][=2019-04-08 02:11=]] Agregar archivos sin commitear :S
