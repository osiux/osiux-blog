#+TITLE:       monitoreo en status bar de =tmux=
#+DESCRIPTION: Usar spark en la status bar de tmux para para monitorear
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    SysAdmin, config, monit, qmail, spark, tmux
#+DATE:        2014-11-14 03:03
#+HTML_HEAD:   <meta property="og:title" content="monitoreo en status bar de =tmux=" />
#+HTML_HEAD:   <meta property="og:description" content="Usar spark en la status bar de tmux para para monitorear" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2014-11-14" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2016-02-03-monitoreo-en-status-bar-de-tmux.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/qmhandle-status-bar-tmux.png" />


#+ATTR_HTML: :width 570 :height 147 :title qmhandle status bar tmux using spark
[[file:img/qmhandle-status-bar-tmux.png][file:tmb/qmhandle-status-bar-tmux-spark.png]]

** ...anda lento... mmm... que raro!

   Hace un par de meses que heredé un par de servidores con varios
   servicios que deben estar operativos 24x7 y entre ellos me tocó
   manter un /qmail/ y como no podía ser de otra manera de tanto en
   tanto hay algo de SPAM y por ello la cola de mails se eleva. :S

   A fin de tener alguna métrica de qué está sucediendo era necesario
   visualizar constantemente el estado de la cola y si bien hay
   muchísimas herramientas disponibles opté por el principio
   *KISS* [fn:KISS] (/Keep it simple, stupid/), de momento sólo lo
   vería yo y posiblemente alguién más en mi ausencia, no quería
   instalar nada en ese servidor, tampoco tenía muchas posibilidades
   de conectarme a otros servidores por reglas de firewall y quería
   tenerlo visible en mi tmux ya que paso ahí gran parte del día (y a
   veces las noches)

** =bash= para generar y =spark= para graficar

   Abusando una vez más de /bash/ hice un script en el server que
   envía un timestamp y metricas básicas, entre ellas la cantidad de
   mails encolados a un log y ese log se copia un una vps donde esta
   este blog, asi de esta manera el qmail envía a osiux.com el log y
   luego un script debería descargar de osiux.com para "graficar"
   localmente.

   El contenido del script =qmhandle=:

   #+BEGIN_SRC sh :session :results none :export code
      #!/bin/sh
      export http_proxy=""
      log=/tmp/qmhandle.log
      wget -q -O $log http://pub.osiux.com/qmhandle.log
      q=`tail -20 $log | awk '{print $5}'`

      a=`echo $q | awk '{print \$1}'`
      b=`echo $q | awk '{print \$5}'`
      c=`echo $q | awk '{print \$10}'`
      d=`echo $q | awk '{print \$15}'`
      e=`echo $q | awk '{print \$20}'`

      echo $a `echo $a $b $c $d $e | spark` $e
   #+END_SRC

   Si ejecutamos =qmhandle= vemos:

   : 531 █▄▄▁▂ 524

   Para entender qué estamos visualizando, miremos el log:

   #+BEGIN_SRC sh :session :results none :exports code
     tail /tmp/qmhandle.log
   #+END_SRC

    #+begin_example
    2016-02-03 15:10 527 1 526 1 0
    2016-02-03 15:11 524 0 524 1 0
    2016-02-03 15:12 523 0 523 1 0
    2016-02-03 15:13 523 0 523 1 0
    2016-02-03 15:14 523 1 522 1 1
    2016-02-03 15:15 523 0 523 1 0
    2016-02-03 15:16 521 0 521 1 0
    2016-02-03 15:17 526 0 526 1 0
    2016-02-03 15:18 523 0 523 1 0
    2016-02-03 15:19 524 0 524 1 0
    #+end_example

    El resultado es un /timestamp/ con una serie de números, la
    columna 5 es la que importa y hay una línea nueva cada 1 minuto.

    Al procesar el log, me quedo con la cantidad de mails encolados
    cada 1, 5, 10, 15 y 20 minutos, asi tengo una tendencia con unas
    bonitas barras (gracias a *spark* [fn:spark]) y el mínimo y
    máximo, esta información se actualiza en la barra de tmux cada 1
    minuto, agregando al =~/.tmux.conf= el comando qmhandle:

    #+BEGIN_EXAMPLE
      set -g status-right '#[fg=red,bg=default]#(qmhandle) #[fg=colour231,bg=colour232] #(bateria) #[fg=colour11,bg=colour232] %d/%m %H:%M'
    #+END_EXAMPLE

    Básico pero efectivo! *:)*

[fn:KISS] https://en.wikipedia.org/wiki/KISS_principle
[fn:spark] https://github.com/holman/spark



** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74b0abbc89a399fba3fabae1cd5434d229ad845c][=2023-05-09 11:37=]]  agregar DESCRIPTION, KEYWORDS y actualizar OpenGraph en /monitoreo en status bar de =tmux=/
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/1f6384ca69090d6834954ad64441e507d14ede0b][=2019-04-18 02:11=]] Actualizar archive, years, charlas y corregir babel en un post
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/882b1cda084f7c18362c8414a78dd65c7b968a3f][=2019-04-09 03:01=]] Recuperar archivos en .md y convertirlos a .org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/1311ba6ddb764547ee6f44b036a1d5fa9c11d15e][=2016-02-03 18:20=]] @ 00:01 hs - mejor barra spark en 2016-02-03-monitoreo-en-status-bar-de-tmux.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/6efd26549d3920141eb203a1572e616760c96405][=2016-02-03 03:07=]] @ 02:00 hs - reorganizo archive blog y agrego primer post del 2016 :)
