#+TITLE:       CampusParty Bogotá CPCO5
#+DESCRIPTION: Resumen CampusParty Bogotá CPCO5
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    Evento, 2012, Bogotá, BuenosAiresLibre, CampusParty, Colombia, FreeNetworks, OpenWRT, RedesLibres, Talks, WiFi
#+DATE:        2012-06-26 22:19
#+HTML_HEAD:   <meta property="og:title" content="CampusParty Bogotá CPCO5" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2012-06-26" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2012-06-26-campus-party-bogota-cpco5.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/cpco5/cpco5-redlibre.png" />

** la previa

El viaje comenzó bien, logré capturar el booteo del sistema de
entretenimiento del avión, el cual usa /GNU/Linux/. Al llegar a
/Corferias/, había una cola enorme intentando registrarse y no quedaban
dudas de que el evento era muy grande!

[[file:img/cpco5/cpco5-1920-01.jpg][file:img/cpco5/cpco5-600-01.jpg]]

El datacenter llamado *cerebro* estaba en medio de la *arena*.

[[file:img/cpco5/cpco5-1920-02.jpg][file:img/cpco5/cpco5-600-02.jpg]]

Hice el reconocimiento del lugar.

[[file:img/cpco5/cpco5-1920-03.jpg][file:img/cpco5/cpco5-600-03.jpg]]

Encontré un canal de televisión que transmite desde el evento y unos
auditorios abiertos muy cómodos.

[[file:img/cpco5/cpco5-1920-04.jpg][file:img/cpco5/cpco5-600-04.jpg]]

Ni bien ubiqué un lugar para conectarme y dar aviso que de estaba todo
bien, recorrí el camping y pude ver que el tráfico de red comenzaba a
aumentar y esto era la previa.

[[file:img/cpco5/cpco5-1920-05.jpg][file:img/cpco5/cpco5-600-05.jpg]]

** el primer día de charlas

El público aumentó considerablemente y había un clima de juegos.

[[file:img/cpco5/cpco5-1920-06.jpg][file:img/cpco5/cpco5-600-06.jpg]]

Aparecieron las clásicas promotoras y los stands dentro y fuera de la
*arena*.

[[file:img/cpco5/cpco5-1920-07.jpg][file:img/cpco5/cpco5-600-07.jpg]]

Encontré robots y amigos.

[[file:img/cpco5/cpco5-1920-08.jpg][file:img/cpco5/cpco5-600-08.jpg]]

** no seas un terminal de internet, creá una red libre

A las 16hs di una conferencia donde comenté las ventajas de las redes
libres y la interconexión con internet ante un público que se animó a
preguntar bastante!

[[file:img/cpco5/cpco5-1920-09.jpg][file:img/cpco5/cpco5-600-09.jpg]]

*** video

#+BEGIN_EXPORT html
<iframe width="560" height="315" src="https://www.youtube.com/embed/IKO17A5Imbc" title="No seas un terminal de internet, creá una red libre!" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
#+END_EXPORT

*** slides

- http://pub.osiux.com/cpco5-redlibre.pdf

** continuará...

Mañana sigue la movida e intentaré descubir la noche del /CampusParty/
ya que es un evento 24hs



** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/46c34b1d33c13bcad46f73e7866e8c5e7a6b893e][=2023-06-07 21:29=]] agregar DESCRIPTION, KEYWORDS, actualizar imagen OpenGraph, corregir sintaxis y agregar iframe video en /CampusParty Bogotá CPCO5/
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/0baa20f806c177591f312d276cd37ac99f057604][=2013-06-05 02:58=]] @ 02:30 hs - responsive max-width css + refactor
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/00977674c60135495c431e80cae66c249d08aa42][=2012-12-16 09:50=]] @ 00:43 hs - Corrijo formato archivos.
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
