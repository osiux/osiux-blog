#+TITLE:       Hardware Freedom Day 2014
#+AUTHOR:      Osiris Alejandro Gómez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+DATE:        2014-03-12 09:25
#+HTML_HEAD:   <meta property="og:title" content="Hardware Freedom Day 2014" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="2014-03-12" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gómez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2014-03-12-hardware-freedom-day-2014.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/Hardware-Freedom-Day-2014.png" />


#+ATTR_HTML: :width 640 :height 346 :title Hardware Freedom Day 2014
[[file:img/Hardware-Freedom-Day-2014.png][file:tmb/Hardware-Freedom-Day-2014.png]]

El próximo Sábado 15 de Marzo en el Aula 60 de la Universidad Nacional
de Quilmes (Roque Saénz Peña 352, Bernal, Quilmes, Pabellón Aulas Sur,
Planta Baja) Se realizará el Día de la Libertad del Hardware
Edición 2014.

El evento se realizará en simultáneo en más de 20 países de todo el mundo.

La importancia del Hardware Abierto se ha ido incrementando los
últimos años, con la aparición de dispositivos cada vez más poderosos.

La filosofía del software libre (las ideas sobre la libertad edl
conocimiento) se aplican directamente a los diseños de hardware,
permitiendo así copiarlos, redistrivuirlos, modificarlos y compartir
las mejoras, de manera que beneficien a toda la comunidad.

Este año contaremos con la presencia de destacados panelistas que
compartirán sus experiencias en el desarrollo de tecnologías de HL.

Las acreditaciones son a partir de las 13:45, las charlas comienzan a
las 14:00hs y se extenderán hasta las 18:00hs.

Los esperamos.

Cualquier consulta o sugerencia es bien recibida, también a hfday2014@quilmeslug.org

Algunas de las Presentaciones/Charlas:

| Arduino YUN                                    | Federico Pfaffendorf      | [[http://clubarduino.com.ar/][Club Arduino - Argentina]]                  |
| Balero Rodante                                 | Diego Gutierrez (Diegote) | [[http://buenosaireslibre.org][BuenosAiresLibre]]                          |
| Brazos, robots, insectos                       | Daniel Esquerdo           | [[http://www.kitrobot.com.ar/][Kit Robot]]                                 |
| Crear - El Arte y la Tecnología                | Facundo Mainere           | [[http://www.elarteylatecnologia.com.ar/][Cooperativa de Trabajo]]                    |
| OpenMolo                                       | Leonel Caraciolli         | [[http://openmolo.com.ar/][OpenMolo]]                                  |
| Robótica con Hardware Abierto y Software Libre | Martin Noblia             | [[http://www.unq.edu.ar/][Proyecto de Extensión Universitaria RHASL]] |

** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/a2abb3b333c91e0d3c15ea9e93a21589bd4d86f7][=2015-07-03 03:59=]] @ 01:00 hs - reemplazo :alt por :title y cambios menores
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/1a38832e534e461966f6761276c58408d403b253][=2014-03-12 09:12=]] @ 00:14 hs - Agrego Hardware Freedom Day 2014
