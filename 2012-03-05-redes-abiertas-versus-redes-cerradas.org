#+TITLE:       redes abiertas versus redes cerradas
#+DESCRIPTION: Capturar y graficar los Access Points de las redes WiFi cercanas
#+AUTHOR:      Osiris Alejandro Gomez
#+EMAIL:       osiux@osiux.com
#+LANGUAGE:    es
#+LINK_HOME:   index.html
#+INCLUDE:     header.org
#+KEYWORDS:    SysAdmin, WiFi, GraphViz, Wireless, aps2dot, RedesLibres
#+DATE:        2012-03-05 22:19
#+HTML_HEAD:   <meta property="og:title" content="redes abiertas versus redes cerradas" />
#+HTML_HEAD:   <meta property="og:description" content="Capturar y graficar los Access Points de las redes WiFi cercanas" />
#+HTML_HEAD:   <meta property="og:type" content="article" />
#+HTML_HEAD:   <meta property="og:article:published_time" content="" />
#+HTML_HEAD:   <meta property="og:article:author" content="Osiris Alejandro Gomez" />
#+HTML_HEAD:   <meta property="og:url" content="https://osiux.com/2012-03-05-redes-abiertas-versus-redes-cerradas.html" />
#+HTML_HEAD:   <meta property="og:site_name" content="OSiUX" />
#+HTML_HEAD:   <meta property="og:locale" content="es_AR" />
#+HTML_HEAD:   <meta property="og:image" content="https://osiux.com/img/aps-2012-03-05.gif" />

Estuve jugando un poco con varios /APs/ (/Access Points/) y luego de un
par de minutos de /scannear/ las redes que me rodean se me ocurrió
graficarlas!

Como de costumbre la herramienta elegida fue =graphviz= [fn:graphviz]] y
salió bastante bien, comparto la imagen:

[[file:img/aps-2012-03-05.gif]]

A medida que la señal disminuye los /SSIDs/ (nombres) se alejan del
centro (mi nodo) y el tamaño de la fuente se reduce.

Como se puede apreciar, lamentablemente no abundan las redes abiertas,
por el contrario, escasean y lo peor de todo es que muy pocas asignan
/IP/ por /DHCP/ y cuentan con /cero contenido/, algunos pocos dan el
servicio de salir a /INET/.

Obviamente la excepción son los pocos nodos
de =BuenosAiresLibre= [fn:BAL]. Sería interesante poder revertir esta
situación y lograr que los 309 equipos listados sean cada uno parte de
una /red libre, abierta y comunitaria/, donde todos podríamos sacar
provecho de la misma, dejando de estar aislados y cerrados con la
comunidad que nos rodea.

Invito a todos a capturar su zona y ver en un par de meses o años si
el gráfico se torna más verde que rojo.

Solo hace falta generar un archivo que contenga /SSID/, /MAC/, estado
(abierta o cerrada) y el nivel de señal, como por ejemplo

#+BEGIN_EXAMPLE

  FT9081668 	e0:cb:4e:61:4a:0f 	6 (B+G) 	AP 	yes 	50
  ACOYTE 	98:fc:11:d1:45:d1 	9 (B+G) 	AP 	yes 	44
  mesh.buenosaireslibre.org 	f8:d1:11:7a:61:b2 	1 (B+G) 	Ad hoc 	no 	35

#+END_EXAMPLE

Y luego ejecutar el script
[[https://github.com/osiris/aps2dot/blob/master/aps2dot][aps2dot]] de la
siguiente manera:

#+BEGIN_SRC sh :results none :exports code

  ./aps2dot.sh >aps.dot;neato -Tpng aps.dot >aps.png

#+END_SRC

Esta disponible el código fuente de =aps2dot= en =gitlab= [fn:aps2dot]

Se aceptan sugerencias a osiux@osiux.com

Y acordate, /si en tu barrio no hay un nodo libre, es porque *vos* no
lo hiciste!/

[fn:graphviz]  https://graphviz.org/
[fn:aps2dot]   https://gitlab.com/osiux/aps2dot
[fn:BAL]       https://web.archive.org/web/20131004183342/http://wiki.buenosaireslibre.org/



** ChangeLog

  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/cee938d5995fe8345d7b8c02ccf502823c7a0cd4][=2023-05-03 23:15=]] corregir links ChangeLog en 2012-03-05-redes-abiertas-versus-redes-cerradas.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/f16b7ae2cc1335c30dbe45a135a660f2d72ad9ef][=2023-04-29 15:12=]] agregar DESCRIPTION, KEYWORDS, actualizar opengraph, corregir links y formato
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bf3a61526ad2a73cecb77a18995f1d63494e3664][=2022-11-13 20:39=]] agregar y actualizar tags OpenGraph
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e46ec52748a7ecc60f09c3b95e363e92eaa0bebc][=2019-04-18 00:21=]] Agregar hora en header date
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5c8643b83930c6269569c76602608bd33f93008b][=2019-04-18 00:01=]] Corregir identación header #+INCLUDE:
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/bbc3bbc728f2a3eeb4fe2e0a012ee5d8d613e3ef][=2015-07-03 04:31=]] @ 00:05 hs - elimino #+OPTIONS: de todos los archivos excepto header.org
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/74165280ffad770d1f8b8acbfa7f22b95459b52a][=2014-04-22 11:35=]] @ 00:34 hs - Agrego timestamp:nil
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/5ad3755a3df07cdfbdc75d56cae06db2fee4b5f2][=2013-04-24 08:04=]] @ 01:50 hs - migro a org 8.0
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/e4dc30e2a202e7ad44bddc761065e9aeb0a31bd3][=2013-03-21 02:27=]] @ 01:58 hs - agrego cambios según git.osiux.com
  - [[https://gitlab.com/osiux/osiux.gitlab.io/-/commit/652199f438b8e3b7f52720e2dc19208c9bcd7651][=2012-12-15 22:31=]] @ 04:00 hs - convert old blog in rST to org
